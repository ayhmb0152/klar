package remediate

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/moby/buildkit/frontend/dockerfile/parser"
	"github.com/pkg/errors"
	"github.com/pmezard/go-difflib/difflib"
	log "github.com/sirupsen/logrus"
)

var (
	// ErrNoDockerfile occurs when the Dockerfile can't be found
	ErrNoDockerfile = errors.New("Dockerfile does not exist")

	errDockerInsertionPointNotFound = errors.New("unable to determine diff insertion point for Dockerfile, can't find `FROM` directive")
)

// Remediator holds the details necessary to generate a Diff against
// the given dockerfile
type Remediator struct {
	dockerfileLines  []string
	pathToDockerfile string
	insertionLineNum int
}

// NewRemediator creates a new Remediator object, used for generating a unified diff
func NewRemediator(pathToDockerfile string) (*Remediator, error) {
	dockerfile, err := openDockerfile(pathToDockerfile)
	if err != nil {
		return nil, err
	}
	defer dockerfile.Close()

	insertionLineNum, err := dockerfileInsertionLineNumber(dockerfile)
	if err != nil {
		return nil, err
	}

	lines, err := readLines(dockerfile)
	if err != nil {
		return nil, err
	}

	return &Remediator{
		pathToDockerfile: pathToDockerfile,
		dockerfileLines:  lines,
		insertionLineNum: insertionLineNum,
	}, nil
}

// Diff generates a unified diff which can be be used to patch the Dockerfile
// using `git apply`. This diff includes the necessary operating system commands
// to upgrade the given package and version
func (r *Remediator) Diff(namespaceName, featureName, fixedBy string) (string, error) {
	upgradeCmd, err := generateUpgradeCmd(namespaceName, featureName, fixedBy)

	if err != nil {
		return "", err
	}

	dockerRunCmd := fmt.Sprintf("RUN %s", upgradeCmd)
	return r.generateDiff(dockerRunCmd)
}

func openDockerfile(pathToDockerfile string) (*os.File, error) {
	dockerfile, err := os.Open(pathToDockerfile)
	if err == nil {
		log.Infof("Found Dockerfile with path: '%s', using for remediations.", pathToDockerfile)
		return dockerfile, nil
	}

	if os.IsNotExist(err) {
		return nil, ErrNoDockerfile
	}

	return nil, err
}

func (r *Remediator) generateDiff(upgradeCmd string) (string, error) {
	updatedLines := r.updatedLines(upgradeCmd)

	diff := difflib.UnifiedDiff{
		A:        r.dockerfileLines,
		B:        updatedLines,
		FromFile: filepath.Join("a", r.pathToDockerfile),
		ToFile:   filepath.Join("b", r.pathToDockerfile),
		Context:  3,
	}

	return difflib.GetUnifiedDiffString(diff)
}

func readLines(dockerfile *os.File) ([]string, error) {
	dockerfile.Seek(0, io.SeekStart)

	dockerfileLines := []string{}

	scanner := bufio.NewScanner(dockerfile)
	for scanner.Scan() {
		// need to re-add newline at the end because difflib.GetUnifiedDiffString
		// expects each line to end in a newline
		dockerfileLines = append(dockerfileLines, scanner.Text()+"\n")
	}

	return dockerfileLines, scanner.Err()
}

func (r *Remediator) updatedLines(upgradeCmd string) []string {
	updatedLines := []string{}

	for i, line := range r.dockerfileLines {
		if i == r.insertionLineNum {
			updatedLines = append(updatedLines, []string{upgradeCmd + "\n"}...)
		}
		updatedLines = append(updatedLines, line)
	}

	return updatedLines
}

// dockerfileInsertionLineNumber determines the line number in the Dockerfile where we should
// insert new docker directives
func dockerfileInsertionLineNumber(dockerfile *os.File) (int, error) {
	dockerfile.Seek(0, io.SeekStart)

	// use the Docker parser so we don't need to mess around with
	// regular expressions to find the "FROM" directive
	parsedDockerfile, err := parser.Parse(dockerfile)
	if err != nil {
		return 0, errors.Wrap(err, "Unable to parse Dockerfile")
	}

	// want to set the insertion line after the last "FROM" directive in the Dockerfile
	insertionLineNum := 0
	for _, child := range parsedDockerfile.AST.Children {
		if strings.ToLower(child.Value) != "from" {
			continue
		}

		if child.EndLine > insertionLineNum {
			insertionLineNum = child.EndLine
		}
	}

	if insertionLineNum == 0 {
		return 0, errDockerInsertionPointNotFound
	}

	return insertionLineNum, nil
}
