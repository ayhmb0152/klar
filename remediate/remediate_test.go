package remediate_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/internal/testutils"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/remediate"
)

func TestNewRemediator(t *testing.T) {
	t.Run("when the path to the Dockerfile does not exist", func(t *testing.T) {
		want := remediate.ErrNoDockerfile
		_, err := remediate.NewRemediator("non-existent-Dockerfile")
		if err != want {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, err)
		}
	})

	t.Run("when the path to the Dockerfile exists and the file is invalid", func(t *testing.T) {
		_, err := remediate.NewRemediator(testutils.FixturesPath("Dockerfile-invalid"))
		if err == nil {
			t.Fatal("Expected err, got nil")
		}
	})

	t.Run("when the path to the Dockerfile exists and the file is valid", func(t *testing.T) {
		_, err := remediate.NewRemediator(testutils.PathToDockerfile())
		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}
	})
}

func TestDiff(t *testing.T) {
	debianDiff := `--- testdata/Dockerfile
+++ testdata/Dockerfile
@@ -34,6 +34,7 @@
 RUN go build -o /analyzer
 WORKDIR /go/src/github.com/coreos/clair/
 FROM registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e
+RUN apt-get update && apt-get upgrade -y openssl && rm -rf /var/lib/apt/lists/*
 # used to determine within the container scanning binary whether we're running
 # from within the context of Docker
 ENV RUNNING_FROM_DOCKER "true"
`

	rhelDiff := `--- testdata/Dockerfile
+++ testdata/Dockerfile
@@ -34,6 +34,7 @@
 RUN go build -o /analyzer
 WORKDIR /go/src/github.com/coreos/clair/
 FROM registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e
+RUN yum -y check-update || { rc=$?; [ "$rc" -neq 100 ] && exit $rc; yum update -y openssl; } && yum clean all
 # used to determine within the container scanning binary whether we're running
 # from within the context of Docker
 ENV RUNNING_FROM_DOCKER "true"
`

	alpineDiff := `--- testdata/Dockerfile
+++ testdata/Dockerfile
@@ -34,6 +34,7 @@
 RUN go build -o /analyzer
 WORKDIR /go/src/github.com/coreos/clair/
 FROM registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e
+RUN apk --no-cache update && apk --no-cache add openssl=1.4.9
 # used to determine within the container scanning binary whether we're running
 # from within the context of Docker
 ENV RUNNING_FROM_DOCKER "true"
`

	var cases = []struct {
		Name, OperatingSystem, Diff string
		Err                         error
	}{
		{"when the operating system is unknown", "unknown", "", remediate.OsNotFoundError("unknown")},
		{"when the operating system is debian", "debian", debianDiff, nil},
		{"when the operating system is ubuntu", "ubuntu", debianDiff, nil},
		{"when the operating system is centos", "centos", rhelDiff, nil},
		{"when the operating system is oracle", "oracle", rhelDiff, nil},
		{"when the operating system is alpine", "alpine", alpineDiff, nil},
	}

	for _, tc := range cases {
		t.Run(tc.Name, func(t *testing.T) {
			remediator, err := remediate.NewRemediator(testutils.PathToDockerfile())
			if err != nil {
				t.Fatalf("Expected no err: %s", err)
			}

			got, err := remediator.Diff(tc.OperatingSystem, "openssl", "1.4.9")
			if !reflect.DeepEqual(err, tc.Err) {
				t.Fatalf("Wrong result. Expected:\n%#v\nbut got:\n%#v", tc.Err, err)
			}

			want := tc.Diff
			if !reflect.DeepEqual(want, got) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
			}
		})
	}
}
