### Components

The [GitLab Container Scanning tool](https://gitlab.com/gitlab-org/security-products/analyzers/klar) consists of the following four components:

1. [clair-db](https://hub.docker.com/r/arminc/clair-db) - a Docker container which runs a PostgreSQL server hosting the vulnerability definitions
1. [clair](https://github.com/quay/clair) - a "vulnerability analysis" server which provides an API endpoint that allows clients to send Docker image data to. Clair then analyses the image by interfacing with the `clair-db` vulnerability database and returns a list of vulnerabilities contained in the image. The `clair server` is compiled in the [Dockerfile](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/Dockerfile#L43)
1. [klar](https://github.com/optiopay/klar) - a tool that can extract Docker image details (such as the individual layers) from a [container registry](https://docs.docker.com/registry/deploying/) and then push them to the `clair server` and retrieve the resulting list of vulnerabilities. The binary for the `klar` tool is downloaded from the remote repository in the [Dockerfile](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/Dockerfile#L38)
1. [analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/klar) - the GitLab produced tool which is responsible for starting up the [clair server](https://github.com/quay/clair) as a background process, then executing the `klar` binary with a Docker image to be analyzed. Once the vulnerability analysis is complete, it creates a `gl-container-scanning-report.json` file using the format defined in the [GitLab Analyzers Common Library](https://gitlab.com/gitlab-org/security-products/analyzers/common). The `analyzer` Go binary is compiled from the source in the [GitLab Container Scanning](https://gitlab.com/gitlab-org/security-products/analyzers/klar) repository in the [Dockerfile](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/Dockerfile#L33).

### Sequence Diagram

Below is a sequence diagram of the process of analyzing a Docker container using the [GitLab Container Scanning tool](https://gitlab.com/gitlab-org/security-products/analyzers/klar), which is referred to as `analyzer` in the diagram:

```mermaid
sequenceDiagram
  participant A as analyzer
  participant K as klar
  participant C as clair
  participant R as registry

  %% https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/analyze/analyze.go#L54
  A->>+K: ./klar alpine:3.10
  %% https://github.com/optiopay/klar/blob/v2.4.0/main.go#L52
  K->>+R: GET image manifest
  %% https://github.com/optiopay/klar/blob/v2.4.0/docker/docker.go#L317
  R->>-K: image manifest
  %% https://github.com/optiopay/klar/blob/v2.4.0/main.go#L74
  Note over K,C: Push layer URLs to clair
  %% https://github.com/optiopay/klar/blob/v2.4.0/clair/api.go#L71
  loop each layer
    K->>+C: POST localhost:6060/v1/layers
    C->>+R: GET layer data
    R->>-C: layer data
    Note over C: analyze layer
    C->>-K: Status: created
  end
  %% https://github.com/optiopay/klar/blob/v2.4.0/clair/clair.go#L123
  Note over K,C: Analyze
  %% https://github.com/optiopay/klar/blob/v2.4.0/clair/api.go#L114
  %% https://github.com/optiopay/klar/blob/v2.4.0/docker/docker.go#L45
  K->>+C: GET AnalyzedLayerName
  C->>-K: vulnerabilities data
  %% https://github.com/optiopay/klar/blob/v2.4.0/main.go#L96
  K->>-A: vulnerabilities JSON
```

### Scanning Process Walkthrough

Let's consider the Container Scanning process from the context of [running the standalone Container Scanning Tool](https://docs.gitlab.com/ee/user/application_security/container_scanning/#running-the-standalone-container-scanning-tool):

1. Run the Container Scanner Docker image:

   ```
   docker run \
     --interactive --rm \
     --volume "$PWD":/tmp/app \
     -e CI_PROJECT_DIR=/tmp/app \
     -e CLAIR_DB_CONNECTION_STRING="postgresql://postgres:password@your.local.ip:5432/postgres?sslmode=disable&statement_timeout=60000" \
     -e CI_APPLICATION_REPOSITORY=alpine \
     -e CI_APPLICATION_TAG=3.7 \
     registry.gitlab.com/gitlab-org/security-products/analyzers/klar
   ```

1. The `CMD` directive inside the above Container Scanner Docker image [will execute a wrapper script](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/2fc8cd76281c63336d6a9122a679e0816b114527/Dockerfile#L71) called `/container-scanner/start.sh` (which exists for backwards compatibility but will be removed in a future release):

   ```
   CMD ["/usr/bin/dumb-init", "/container-scanner/start.sh"]
   ```

1. This wrapper script executes the actual analyzer:

   ```
   #!/usr/bin/env sh
   /analyzer r
   ```

1. The `analyzer` now sets a number of [environment variables](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/environment/environment.go), including the [`CLAIR_ADDR`](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/environment/environment.go#L79) environment variable which `klar` uses when it sends a `POST` request with layer data to `clair`:

   ```go
   os.Setenv("CLAIR_ADDR", "localhost")
   ```

   **Note:** Since both `klar` and `clair` are running from within the context of the Docker container, the `CLAIR_ADDR` value has been hardcoded to `localhost`, which is the address that the `clair` server will be reachable _from inside the Docker container_.  Also, because this value has been hardcoded, it doesn't matter if you pass an environment variable to the Docker container in `step 1.` such as `-e CLAIR_ADDR="some-address"`, as it will be overridden at this point.

1. Now that the environment variables have been configured, the `analyzer` now [starts the clair server in the background](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/analyze/analyze.go#L31):

   ```go
   // start Clair server process in the background
   err := clair.StartBackgroundServer(startClairServer, clairVulnerabilitiesDBConnectionStr)
   ```

1. Once the `analyzer` has confirmed that the `clair server` is running and accepting incoming API connections, we [execute the `klar` binary](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/analyze/analyze.go#L45), and pass it the environment variables that have previously been configured in `step 4.`

1. The `klar` binary [fetches the manifest](https://github.com/optiopay/klar/blob/v2.4.0/docker/docker.go#L317) for the provided Docker image, [loops through all of the layers](https://github.com/optiopay/klar/blob/v2.4.0/clair/api.go#L71) specified in the manifest, and [sends a `POST`](https://github.com/optiopay/klar/blob/v2.4.0/clair/api.go#L85) request containing the following request body to the `CLAIR_ADDR` value which is configured in `step 4.`:

   ```
   POST /v1/layers HTTP/1.1
   Host: localhost:6060
   Content-Type: application/json
   ```

   ```json
   {
     "Layer": {
       "Name": "parent-layer-id-concatenated-with-sha256-digest-of-the-layer-data",
       "Path": "http://ip.address.of.registry:5000/v2/ubi/blobs/sha256-digest-of-the-layer-data",
       "Format": "Docker"
     }
   }
   ```

   **Note:** The above `POST` request uses the hostname from the hardcoded `CLAIR_ADDR` value, which is `localhost`. `klar` automatically inserts the procol `http` and the port number `6060` as described [here](https://github.com/optiopay/klar#usage).

1. `Clair` then fetches the actual layer data directly from the registry using the `Path` component of the `POST` request sent in the above step. After retrieving the layer data, it performs a vulnerability analysis of the layer using the [clair-db](https://hub.docker.com/r/arminc/clair-db) vulnerability database and returns the following response:

   ```
   Status: Created (201)
   ```

   ```json
   {
     "Layer":{
       "Name": "parent-layer-id-concatenated-with-sha256-digest-of-the-layer-data",
       "Path": "http://ip.address.of.registry:5000/v2/ubi/blobs/sha256-digest-of-the-layer-data",
       "Format":"Docker",
       "IndexedByVersion":3
     }
   }
   ```

1. `klar` then [fetches the vulnerability data](https://github.com/optiopay/klar/blob/v2.4.0/clair/api.go#L114) by issuing a `GET` request to the `Path` component provided in the above step (which matches the same `Path` component sent by `klar` in `step 7.`).  After the vulnerability data has been fetched, `klar` performs the following steps:

   1. [Converts the data](https://github.com/optiopay/klar/blob/v2.4.0/clair/api.go#L133-L144)
   1. [Filters it](https://github.com/optiopay/klar/blob/v2.4.0/main.go#L91)
   1. [Sorts it](https://github.com/optiopay/klar/blob/v2.4.0/main.go#L93)
   1. Converts it into [`JSON` format](https://github.com/optiopay/klar/blob/v2.4.0/main.go#L96) and returns it

1. The `analyzer` then takes this `JSON` data, and does the following:

   1. [Converts it](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/clicommand/clicommand.go#L216) into the format defined in the [GitLab Analyzers Common Library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
   1. [Generates vulnerability remediation data](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/convert/convert.go#L30)
   1. [Removes any vulnerabilities found in the allowlist](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/clicommand/clicommand.go#L222)
   1. [Sorts the data](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/clicommand/clicommand.go#L228)
   1. [Outputs the vulnerability data in table form](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/clicommand/clicommand.go#L231)
   1. [Creates a `gl-container-scanning-report.js` file](https://gitlab.com/gitlab-org/security-products/analyzers/klar/-/blob/v2.3.1/clicommand/clicommand.go#L232)
