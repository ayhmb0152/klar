package urls

const (
	// NewIssue provides the URL for creating a new GitLab issue. This is used in various
	// places when an error occurs and we want to prompt the user to submit a bug report.
	NewIssue             = "https://gitlab.com/gitlab-org/gitlab/issues/new?issue"
	containerScanningDoc = "https://docs.gitlab.com/ee/user/application_security/container_scanning/"
	// AvailableVariables provides a url to the Container Scanning Documentation for the available variables section
	AvailableVariables = containerScanningDoc + "#available-variables"
	// Requirements provides a url to the Container Scanning Documentation for the requirements section
	Requirements = containerScanningDoc + "#requirements"
)
