# klar analyzer

GitLab Analyzer for Docker Containers.

This analyzer is a wrapper around [clair](https://github.com/coreos/clair/), a vulnerability static analysis for docker containers, utilizing [klar](https://github.com/optiopay/klar) to analyze images stored in a private or public Docker registry for security vulnerabilities.

## Environment Variables

See the [Available variables](https://docs.gitlab.com/ee/user/application_security/container_scanning/#available-variables) section in the GitLab Container Scanning docs.

## Development

### Build image

```sh
docker build -t container-scanning .
```

### Running the analyzer locally

See the [Running the standalone Container Scanning Tool](https://docs.gitlab.com/ee/user/application_security/container_scanning/#running-the-standalone-container-scanning-tool) section in the GitLab Container Scanning docs.

### Tests

To run the tests:

1. Unit tests
```sh
go test ./...
```
1. Integration tests
```sh
CLAIR_DB_CONNECTION_STRING="postgresql://postgres:password@${LOCAL_MACHINE_IP_ADDRESS}:5432/postgres?sslmode=disable&statement_timeout=60000" ./test.sh
```

## How to update the upstream Scanner

1. Check for the latest versions of `clair` and `klar` at https://github.com/coreos/clair/tags and https://github.com/optiopay/klar/tags
1. Compare with the values of `CLAIR_REPO_TAG` and `KLAR_EXECUTABLE_VERSION` in the [Dockerfile](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/03316e69fbd4af790f512119afba53a84e9c23e7/Dockerfile#L19-23)
1. If an update is available, create a branch and bump the version in [CHANGELOG.md](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/03316e69fbd4af790f512119afba53a84e9c23e7/CHANGELOG.md#L3)
1. Edit the [Dockerfile](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/dada0b5243f0d988a56e2c6c9d07ad20d833b860/Dockerfile) and change the default values for the following Docker build arguments:
      1. [CLAIR_REPO_TAG](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/dada0b5243f0d988a56e2c6c9d07ad20d833b860/Dockerfile#L15)
      1. [KLAR_EXECUTABLE_VERSION](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/dada0b5243f0d988a56e2c6c9d07ad20d833b860/Dockerfile#L16)
      1. [KLAR_EXECUTABLE_SHA](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/dada0b5243f0d988a56e2c6c9d07ad20d833b860/Dockerfile#L17)
1. If updating the `CLAIR_REPO_TAG` variable, also make sure to update the [container-scanner/clair/config.yaml.template](https://gitlab.com/gitlab-org/security-products/analyzers/klar/blob/28b5dd413d804299448440baf79df62eb1650fc0/clair-config.yaml.template) file to match the latest version of the [config.yaml.sample](https://github.com/quay/clair/blob/162e8cdafc66be28b021f83da736a2b612ddda99/config.yaml.sample) from the clair repository, since the format of this file may change between versions.  You'll also need to ensure that the `POSTGRES-VULNERABILITIES-DB-URL` placeholder variable is inserted into the `host` field of the `database` block in the new `config.yaml.template` file, for example:
    ```yaml
    clair:
      database:
        type: pgsql
        options:
          source: POSTGRES-VULNERABILITIES-DB-CONNECTION-STRING
    ```
1. Create a merge request which will automatically build and tag a new analyzer image using the following form: `registry.gitlab.com/gitlab-org/security-products/analyzers/klar/tmp:af864bd61230d3d694eb01d6205b268b4ad63ac0` where the tag is the `SHA` of the most recent commit
1. Create a new branch in the [container-scanning test project](https://gitlab.com/gitlab-org/security-products/tests/container-scanning) and do the following:
      1. Modify the [container_scanning section of .gitlab-ci.yml](https://gitlab.com/gitlab-org/security-products/tests/container-scanning/blob/d5ec8d5f0ce827fd6a0b0017e657a1302b278e89/.gitlab-ci.yml#L21-27) to reference the new analyzer image:
          ```
          container_scanning:
            allow_failure: false
            # the following is the only line you should need to add here
            image: registry.gitlab.com/gitlab-org/security-products/analyzers/klar/tmp:af864bd61230d3d694eb01d6205b268b4ad63ac0
            variables:
              GIT_STRATEGY: fetch
              CLAIR_DB_IMAGE_TAG: "2019-09-04"
            artifacts:
              paths: [gl-container-scanning-report.json]
          ```
      1. Trigger the pipeline for the above branch in the `container-scanning test project` and make sure it passes
1. Merge the request created in step `5.` and follow the [release process](#versioning-and-release-process) to publish this update.

## Versioning and release process

With the initial release of the Klar analyzer in `12.3`, the associated vendored template was using a `12-3-stable` docker image tag. This [has been removed](https://gitlab.com/gitlab-org/gitlab/merge_requests/18343) and starting with GitLab `12.4`, it follows the usual release process as any other analyzer and doesn't need to be released as an `x-y-stable` docker image tag.

Please check the common [Versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see
the [LICENSE](LICENSE) file.
