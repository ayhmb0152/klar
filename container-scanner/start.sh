#!/usr/bin/env sh
set -e

# this script is required in order to maintain backwards compatibility with the
# script section of the `Container-Scanning.gitlab-ci.yml` template and will be
# removed in a future release
# see: https://gitlab.com/gitlab-org/gitlab/blob/30522ca8b901223ac8c32b633d8d67f340b159c1/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml#L24
/analyzer r
